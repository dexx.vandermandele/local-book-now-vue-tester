function dTify(dataTestValue) {
  return `[data-test="${dataTestValue}"]`;
}

/** Retrieves an element based on its data-test attribute.
 * @param {String} dataTestValue The value of the desired element's data-test attribute
 * @param {Object} [parentElement] If provided, only descendants of the parent element will be returned
 */
function $dt(dataTestValue, parentElement) {
  if (parentElement) {
    return parentElement.$(dTify(dataTestValue));
  } else {
    return $(dTify(dataTestValue));
  }
}

module.exports = { dTify, $dt };
