const assert = require('assert');

const { $dt } = require('../helpers');

describe('Selecting and manipulating elements using the data-test selector method', () => {
  before(() => {
    browser.url('https://localhost:3000/fpp/123');
  });

  after(() => {
    console.log('Test will close in five seconds');
    browser.pause(5000);
  });

  it('should perform seamlessly, assuming the FPP Vue app is running locally', () => {
    // Retrieve the recommended hours question parent element
    const recommendedHours = $dt('recommended-hours');
    // Find the edit button within the parent element and click it to enable the hours drop-down
    $dt('recommended-hours-edit-button', recommendedHours).click();
    // Find the drop-down within the parent element and select option 3
    $dt('duration_minutes', recommendedHours).selectByAttribute('data-test', '3');
    // Verify that a warning element has appeared within the parent element
    assert.equal($dt('recommended-hours-warning', recommendedHours).getText(), 'We recommend scheduling at least 3 hours. ');
  });
});
