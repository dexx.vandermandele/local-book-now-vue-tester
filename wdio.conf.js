exports.config = {
  runner: 'local',
  specs: [
    './src/specs/*',
  ],
  exclude: [
  ],
  maxInstances: 1,
  capabilities: [{
    maxInstances: 1,
    browserName: 'chrome',
    acceptInsecureCerts: true,
  }],
  logLevel: 'error',
  logLevels: {
    webdriver: 'error',
    '@wdio/cli': 'error',
  },
  bail: 0,
  waitforTimeout: 10000,
  connectionRetryTimeout: 120000,
  connectionRetryCount: 3,
  services: ['chromedriver'],
  framework: 'mocha',
  reporters: ['spec'],
  mochaOpts: {
    ui: 'bdd',
    timeout: 60000,
  },
};
