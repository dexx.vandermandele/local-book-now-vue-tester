# Local FPP Vue tester
A bare bones implementation of [WebdriverIO](https://webdriver.io) that targets a locally deployed [Fixed Price Path Vue app](https://git.homeadvisor.com/hardcore-league/vue-fixed-price-path). The goal of the project is to verify that changes made to the Vue app play nicely with test automation without having to deploy to sandbox. For example, while adding test locators to a widget, this project can be used to verify that the correct elements can be selected and manipulated. 

## Getting started
- Make sure the Vue app is running on `localhost:3000` (the default).
- Install dependencies with `npm i`.
- Run the example test with `npm run test:example`.

To perform your own checks, copy `src/example/exampleTest.js` to `src/specs/` (or add your own test file) and run `npm test`. `src/specs/` is the default test directory, but is not checked into Git so that you can check whatever you wish without having to worry about contaminating the repository. 

The example test showcases a small library of helpers that simplifies checking selectors. However, this library can also be bypassed entirely if you prefer to use manually write out your selectors using the standard WDIO syntax. 
